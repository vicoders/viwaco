jQuery(document).ready(function($) {
    /*Equal height for column*/
    $(function() {
        $('.service-form').matchHeight({ property: 'min-height' });
        $('.service-item .col-sm-12').matchHeight({
            property: 'height',
        });
        $('.gallery-item').matchHeight({
            property: 'min-height',
        });
        $('#gallery .wp-posts-carousel-details').matchHeight({
            property: 'min-height',
        });

    });

    /*Submenu trang gioi thieu*/

    var submenu = $('.submenu');
    //Hide submenu
    submenu
        .find('ul')
        .hide();
    submenu.each(function() {
        var $this = $(this);
        var src = $this.find('span img').attr('src');
        var src1 = src.replace('plus.png', 'minus.png');
        $this.click(function() {
            //Change image source
            var src3 = $this.find('span img').attr('src');
            var newsrc = (src3 == src) ? (src1) : (src);
            $this.find('span img').attr('src', newsrc);
            //Make another submenu slideUp when click on an submenu
            $this.siblings('li')
                .find('ul')
                .slideUp(400);
            $this.find('ul').slideToggle(400);
        })
    })

    /*Hover menu*/
    if ($(window).width() > 992) {
        $('li.dropdown').hover(function() {
            $(this).find('.dropdown-menu').stop(true, true).show();
        }, function() {
            $(this).find('.dropdown-menu').stop(true, true).hide();
        })
    } else {
        $('li.dropdown').click(function() {
            $(this).toggleClass('open');
        });
    }


    /*AJAX PAGINATION */
    $(document).on('click', '.no-reload-1 .pagination a', function(e) {
        e.preventDefault();
        var link = $(this).attr('href');

        $('#tab1').html('Loading...');
        $('#tab1').load(link + ' .no-reload-1');


    });

    $(document).on('click', '.no-reload-2 .pagination a', function(e) {
        e.preventDefault();
        var link = $(this).attr('href');

        $('#tab2').html('Loading...');
        $('#tab2').load(link + ' .no-reload-2');


    });

    $(document).on('click', '.no-reload-3 .pagination a', function(e) {
        e.preventDefault();
        var link = $(this).attr('href');

        $('#tab3').html('Loading...');
        $('#tab3').load(link + ' .no-reload-3');


    });

    $(document).on('click', '.no-reload-4 .pagination a', function(e) {
        e.preventDefault();
        var link = $(this).attr('href');

        $('#tab4').html('Loading...');
        $('#tab4').load(link + ' .no-reload-4');


    });

    $(document).on('click', '.no-reload-5 .pagination a', function(e) {
        e.preventDefault();
        var link = $(this).attr('href');

        $('#tab5').html('Loading...');
        $('#tab5').load(link + ' .no-reload-5');


    });

    $(document).on('click', '.no-reload-6 .pagination a', function(e) {
        e.preventDefault();
        var link = $(this).attr('href');

        $('#tab6').html('Loading...');
        $('#tab6').load(link + ' .no-reload-6');
    })

    /*CONTACT FORM 7 REMOVE P TAG*/
    $(".wpcf7").find("p").remove();
});
