<?php 
	/*Template Name: Thư viện video*/
	get_header();
 ?>
<div id="img-gallery" >
 	<div class="container">
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo home_url(); ?>">Trang chủ </a>
            </li>
            <li class="active"><?php the_title()?></li>
        </ol>
    </div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-9">
				<div class="row">
					<?php 
						$myposts = get_post_from_postype(-1, 'thu_vien_video');
						if ($myposts) {
			                foreach ($myposts as $post):
			                    setup_postdata($post);?>
			            		<div class="col-sm-4 ">
			            			<div class="gallery-item thumbnail">
										<a href="<?php the_permalink();?>">
				                            <?php if (has_post_thumbnail()) {
				                                the_post_thumbnail();
				                            } ?>
				                            <figcaption class="title" href="<?php the_permalink();?>"><?php the_title()?></figcaption>
				                        </a>

				                        
			                        </div>
								</div>
								<?php
			                endforeach;
			                wp_reset_postdata();
			            }
					?>
					
				</div>
			</div> <!-- end sm 9 -->

			<?php get_sidebar('other'); ?>
		</div><!-- end row -->
		
	</div> <!-- end container fluid -->
	
</div> <!-- end img gallery -->

 <?php get_footer(); ?>