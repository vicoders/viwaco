<aside class="col-sm-3">
    <div class="row">
        <div class="col-xs-6 photo">
           <img src="<?php echo get_template_directory_uri(); ?>/dist/img/photo1.png" alt=""> 
        </div>

        <div class="col-xs-6 photo">
            <img src="<?php echo get_template_directory_uri(); ?>/dist/img/photo2.png" alt="">
        </div>
    </div>
    <div id="payment" class="sidebar-style">
        <h2>thanh toán điện tử</h2>
        <ul>
            <li><a href="" title="">Hướng dẫn đăng ký Payoo</a></li>
            <li><a href="" title="">Phát triển thanh toán không dùng tiền mặt</a></li>
            <li><a href="" title="">Hướng dẫn giao dịch thanh toán qua PAYOO</a></li>
        </ul>
    </div>
    
    <!-- end payment -->
    <div id="map" class="sidebar-style">
        <h2>bản đồ vị trí</h2>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6481209470253!2d105.8020842143078!3d21.00673779390705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135aca1f27d1b5d%3A0x51c5f72058d51810!2zQ8O0bmcgVHkgQ-G7lSBQaOG6p24gVklXQUNP!5e0!3m2!1svi!2s!4v1490346344332" width="100%" height="209" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <!-- end map -->
    <div id="multimeter" class="sidebar-style">
        <h2>đồng hồ điện tử</h2>
        <div style="overflow:hidden">
            <img src="<?php echo get_template_directory_uri() ?>/dist/img/dong-ho.png" alt="">
        </div>
    </div>
    <!-- end multimeter -->
    <div id="gis" class="sidebar-style">
        <h2>hệ thống gis</h2>
        <div style="overflow:hidden">
            <img src="<?php echo get_template_directory_uri() ?>/dist/img/gis.png" alt="">
        </div>
    </div>
    <!-- end gis -->

    <!--====  Gallery  ====-->
    <section id="gallery">
        <div class="gallery-item sidebar-style">
            <h2><a href="<?php echo get_page_uri(get_ID_by_page_name('thu-vien-anh'))?>">Thư viện ảnh</a></h2>
            <?php dynamic_sidebar('sidebar-5') ?>
        </div>
        
        <div class="gallery-item sidebar-style">
            <h2><a href="<?php echo get_page_uri(get_ID_by_page_name('thu-vien-video'))?>">Thư viện video</a></h2>
            <?php dynamic_sidebar('sidebar-6') ?>
        </div>
    </section>
    <!-- end gallery --> 
</aside>
<!-- end sm3 -->