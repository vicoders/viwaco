<?php
/*Template Name: Contact Page*/
get_header();
?>

<!--==========================
    =            Main            =
    ===========================-->
<?php layerslider(1)?>


        <div class="container-fluid">
            <div class="row">
                <section id="contact">
                    <h2><span><img src="<?php echo get_template_directory_uri() ?>/dist/img/contact/icon.png" alt="Lien lac"></span>THÔNG TIN LIÊN HỆ</h2>
                    <div class="col-sm-6">
                        <p><strong>CÔNG TY CỔ PHẦN VIWACO</strong></p>
                        <p>VIWACO JOINT STOCK COMPANY.</p>
                        <p>VIWACO., JSC</p>
                        <p>Địa chỉ trụ sở chính : Tầng 1, nhà 17 – T7 Khu đô thị Trung Hoà – Nhân Chính, Phường Nhân chính, Quận Thanh Xuân, Hà Nội.</p>
                        <p>Điện thoại: (84.4) 62511520, 62511523 <span>Fax : (84.4) 62511524</span></p>
                        <p>Website: <a href="http://viwaco.vn" title="viwaco">http://viwaco.vn </a><span>Email : <a href="" title="">info@viwaco.vn</a></span></p>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6481209470253!2d105.8020842143078!3d21.00673779390705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135aca1f27d1b5d%3A0x51c5f72058d51810!2zQ8O0bmcgVHkgQ-G7lSBQaOG6p24gVklXQUNP!5e0!3m2!1svi!2s!4v1490346344332" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                    
                    <!-- <div class="col-sm-6">
                    
                            <?php // echo do_shortcode('[contact-form-7 id="355" title="Contact form 1"]'); ?>
                    
                        end form
                    </div> -->
                    
                </section>
                <!-- end contact -->
            </div>
            <!-- end row -->
        </div>

<?php get_footer();?>