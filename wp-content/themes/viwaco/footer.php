<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package viwaco
 */

?>
    <section id="partner">
        <div class="container-fluid">
            <h2>các đối tác</h2>
            <?php dynamic_sidebar('sidebar-4')?>
            <!-- end slider -->
        </div>
    </section>
    <!-- end partner -->
    <!--============================
    =            Footer            =
    =============================-->
	<footer>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4 about">
                    <h2>công ty cổ phần Viwaco</h2>
                    <p><span><img src="<?php echo get_template_directory_uri() ?>/dist/img/footer/address.png" alt=""></span>Tầng 1 nhà 17T7 khu đô thị Trung Hòa - Nhân Chính, phường Nhân Chính, quận Thanh Xuân, Hà Nội</p>
                    <p><span><img src="<?php echo get_template_directory_uri() ?>/dist/img/footer/phone.png" alt=""></span>Hotline: 0981923715 04.62511520/62962417</p>
                    <p><span><img src="<?php echo get_template_directory_uri() ?>/dist/img/footer/mail.png" alt=""></span>info@viwaco.vn</p>
                </div>
                <!-- end about -->
                <div class="col-sm-5 info">
                    <h2>Liên kết nhanh</h2>
                    <ul class="list-inline">
                        <?php  $myposts = get_post_from_postype(-1,'doi_tac');
                        foreach ($myposts as $post):?>
                            <li>
                             <?php if ( has_post_thumbnail() ) : ?>
                                <a href="<?php echo get_field('link_goc'); ?>" title="<?php the_title_attribute(); ?>">
                                    <?php the_post_thumbnail(); ?>
                                </a>
                            <?php endif; ?>
                            </li>
                        <?php endforeach; wp_reset_postdata() ?>
                    </ul>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <select name="" id="input" class="form-control fast-link" onchange="if (this.value) window.location.href=this.value">
                                    <option value="">Các đơn vị trực thuộc</option>
                                    <?php 
                                        $myposts = get_post_from_postype(-1,'don_vi_truc_thuoc');
                                        foreach ($myposts as $post):?>
                                           <option value="<?php echo get_field('link_don_vi'); ?>"><?php the_title(); ?></option>
                                        <?php endforeach; wp_reset_postdata() ?>
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <select name="" id="input" class="form-control fast-link" onchange="if (this.value) window.location.href=this.value">
                                    <option value="">Các đối tác</option>
                                    <?php 
                                        $myposts = get_post_from_postype(-1,'doi_tac');
                                        var_dump($myposts);
                                        foreach ($myposts as $post):?>
                                           <option value="<?php echo get_field('link_goc'); ?>"><?php the_title(); ?></option>
                                        <?php endforeach; wp_reset_postdata() ?>
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                
                </div>
                <!-- end info -->

                

                <?php if (is_home() || is_front_page()): ?>
                    <div class="col-sm-3 statistic">
                        <h2 class="text-center">Thống kê truy cập</h2>
                        <table>
                            <tr>
                                <td><i class="fa fa-user" aria-hidden="true"></i>Tổng lượng truy cập</td>
                                <td>: <?php echo wp_statistics_visit('total'); ?></td>
                            </tr>
                            <tr>
                                <td><i class="fa fa-calendar-minus-o" aria-hidden="true"></i>Lượng truy cập hôm nay</td>
                                <td>: <?php echo wp_statistics_visit('today'); ?></td>
                            </tr>
                            <tr>
                                <td><i class="fa fa-desktop " aria-hidden="true"></i>Đang online</td>
                                <td>: <?php echo wp_statistics_useronline(); ?></td>
                            </tr>
                        </table>
                    </div>
                <?php endif;?>

            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
        <div class="copyright">
            <p>Copyright © 2017 Công ty cổ phần viwaco. All Rights Reserved.</p>
            <p>Designed by <a href="http://vmms.vn/" title="VMMS">VMMS</a></p>
        </div>


    </footer>
    <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
 <!--====  /Footer  ====-->
<?php wp_footer();?>

</body>
</html>
