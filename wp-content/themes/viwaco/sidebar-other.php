<aside class="col-sm-3" class="other-sidebar">
    <?php dynamic_sidebar('sidebar-1')?>
    <div class="hot-line">
        <div class="image">
            <img src="<?php echo get_template_directory_uri() ?>/dist/img/duong-day-nong.png" alt="duong-day-nong">
        </div>
    </div>
    <div class="service-item">
        <a href="" title="">
            <div class="row">
                <div class="col-xs-3">
                    <img src="<?php echo get_template_directory_uri() ?>/dist/img/dichvu/file.png" alt="file">
                </div>
                <div class="col-xs-9 text-left">Thủ tục sang tên đồng hồ nước</div>
            </div>
        </a>
    </div>
    <div class="service-item">
        <a href="" title="">
            <div class="row">
                <div class="col-xs-3">
                    <img src="<?php echo get_template_directory_uri() ?>/dist/img/dichvu/contract.png" alt="t">
                </div>
                <div class="col-xs-9 text-left">Đăng ký gắn đồng hồ mới</div>
            </div>
        </a>
    </div>
    <!-- end sm4 -->
    <div class="service-item">
        <a href="" title="">
            <div class="row">
                <div class="col-xs-3">
                    <img src="<?php echo get_template_directory_uri() ?>/dist/img/dichvu/thanh-toan.png" alt="t">
                </div>
                <div class="col-xs-9 text-left">Hình thức thanh toán tiền nước</div>
            </div>
        </a>
    </div>
    <!-- end sm4 -->
    <div class="service-item">
        <a href="" title="">
            <div class="row">
                <div class="col-xs-3">
                    <img src="<?php echo get_template_directory_uri() ?>/dist/img/dichvu/huong-dan.png" alt="folder">
                </div>
                <div class="col-xs-9 text-left">Hướng dẫn đăng ký định mức nước</div>
            </div>
        </a>
    </div>
    <!-- end sm4 -->
    <div class="service-item">
        <a href="" title="">
            <div class="row">
                <div class="col-xs-3">
                    <img src="<?php echo get_template_directory_uri() ?>/dist/img/dichvu/bieu-gia.png" alt="bieugia">
                </div>
                <div class="col-xs-9 text-left">Giá và biểu giá tính tiền nước</div>
            </div>
        </a>
    </div>
    <!-- end sm4 -->
    <div class="service-item">
        <a href="" title="">
            <div class="row">
                <div class="col-xs-3">
                    <img src="<?php echo get_template_directory_uri() ?>/dist/img/dichvu/thay-dong-ho.png" alt="thaydongho">
                </div>
                <div class="col-xs-9 text-left">Thay đồng hồ nước</div>
            </div>
        </a>
    </div>
    <!-- end sm4 -->
    <!-- end payment -->
    <div id="map" class="sidebar-style">
        <iframe class="other" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6481209470253!2d105.8020842143078!3d21.00673779390705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135aca1f27d1b5d%3A0x51c5f72058d51810!2zQ8O0bmcgVHkgQ-G7lSBQaOG6p24gVklXQUNP!5e0!3m2!1svi!2s!4v1490346344332" width="100%" height="243" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>

    <!-- end map -->
</aside>
<!-- end sm3 -->
