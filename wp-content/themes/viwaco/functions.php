<?php
/**
 * viwaco functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package viwaco
 */
// Register custom navigation walker
require_once 'navwalker.php';
// require_once(get_template_directory() . '/widget/mail/Mail.php');

if (!function_exists('viwaco_setup')):
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
    function viwaco_setup()
{
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on viwaco, use a find and replace
         * to change 'viwaco' to the name of your theme in all the template files.
         */
        load_theme_textdomain('viwaco', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus([
            'primary' => esc_html__('Primary', 'viwaco'),
        ]);

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', [
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ]);

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('viwaco_custom_background_args', [
            'default-color' => 'ffffff',
            'default-image' => '',
        ]));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');
    }

endif;
add_action('after_setup_theme', 'viwaco_setup');

function wpd_archive_all_posts( $query ){
    if( $query->is_category() && $query->is_main_query() ) {
        $query->set( 'posts_per_page', 10 );
    }
    return $query;
}
add_action( 'pre_get_posts','wpd_archive_all_posts' );
/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function viwaco_content_width()
{
    $GLOBALS['content_width'] = apply_filters('viwaco_content_width', 640);
}

add_action('after_setup_theme', 'viwaco_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function viwaco_widgets_init()
{
    register_sidebar([
        'name' => esc_html__('Sidebar Other', 'viwaco'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'viwaco'),
        'before_widget' => '<section class="sidebar-custom">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ]);

    register_sidebar([
        'name' => esc_html__('Sidebar Chi Tiet', 'viwaco'),
        'id' => 'sidebar-2',
        'description' => esc_html__('Add widgets here.', 'viwaco'),
        'before_widget' => '<section class="sidebar-custom">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ]);

    register_sidebar([
        'name' => esc_html__('Noibat carousel', 'viwaco'),
        'id' => 'sidebar-3',
        'description' => esc_html__('Add widgets here.', 'viwaco'),
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ]);

    register_sidebar([
        'name' => esc_html__('Partner carousel', 'viwaco'),
        'id' => 'sidebar-4',
        'description' => esc_html__('Add widgets here.', 'viwaco'),
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ]);

    register_sidebar([
        'name' => esc_html__('Image Gallery carousel', 'viwaco'),
        'id' => 'sidebar-5',
        'description' => esc_html__('Add widgets here.', 'viwaco'),
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ]);

    register_sidebar([
        'name' => esc_html__('Video Gallery carousel', 'viwaco'),
        'id' => 'sidebar-6',
        'description' => esc_html__('Add widgets here.', 'viwaco'),
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ]);
}

add_action('widgets_init', 'viwaco_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function viwaco_scripts()
{
    wp_enqueue_style('viwaco-style', get_stylesheet_uri());

    wp_enqueue_script('viwaco-navigation', get_template_directory_uri() . '/js/navigation.js', [], '20151215', true);

    wp_enqueue_script('viwaco-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', [], '20151215', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'viwaco_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Widget.
 */
require get_template_directory() . '/widget/index.php';

/*=======================================
=            CUSTOM FUNCTION            =
=======================================*/

/*SITE_MENU*/
if (!function_exists('main_menu')) {
    function main_menu()
    {
        $args = [
            'menu' => 'primary',
            'depth' => 3,
            'container' => 'false',
            'menu_class' => 'nav navbar-nav',
            'walker' => new wp_bootstrap_navwalker(),
            'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
        ];
        wp_nav_menu($args);
    }
}

/*CHANGE EXCERPT*/
if (!function_exists('modify_excerpt')) {
    function modify_excerpt()
    {
        return '<a href="' . get_the_permalink(get_the_id()) . '" title="">' . __('Xem Tiếp...', 'viwaco') . '</a>';
    }
}

add_action('excerpt_more', 'modify_excerpt');

/*GET THE CURRENT DAY*/
function get_current_weekday()
{
    $weekday = get_the_date("l");
    $weekday = strtolower($weekday);
    switch ($weekday) {
        case 'monday':
            $weekday = 'Thứ hai';
            break;
        case 'tuesday':
            $weekday = 'Thứ ba';
            break;
        case 'wednesday':
            $weekday = 'Thứ tư';
            break;
        case 'thursday':
            $weekday = 'Thứ năm';
            break;
        case 'friday':
            $weekday = 'Thứ sáu';
            break;
        case 'saturday':
            $weekday = 'Thứ bảy';
            break;
        default:
            $weekday = 'Chủ nhật';
            break;
    }
    return $weekday . ', ' . get_the_date('d/m/Y H:i');
}

/*RELATING POST LIST*/
function relating_post_list($post_number, $cat='')
{
    $paged = (get_query_var('page')) ? get_query_var('page') : 1;
    
    if(!empty($cat)){
        $arg = [
            'category_name' => $cat,
            'posts_per_page' => $post_number,
            'paged' => $paged,
        ];
    }else{
        $arg = [
            'post_type' => 'post',
            'posts_per_page' => $post_number,
            'paged' => $paged,
        ];
    }
    
    $query = new WP_Query($arg);
    while ($query->have_posts()): $query->the_post();?>
	    <li class="name"><a href="<?php the_permalink();?>" title=""><time><?php echo '* ' . get_the_date() . " - "; ?></time><?php the_title();?></a></li>
	<?php endwhile;
    wp_pagenavi(['query' => $query]);
    wp_reset_postdata();
}

function relating_post_list_video($post_number)
{
    $paged = (get_query_var('page')) ? get_query_var('page') : 1;
    $arg = [
        'post_type' => 'thu_vien_video',
        'posts_per_page' => $post_number,
        'paged' => $paged,
    ];
    $query = new WP_Query($arg);
      while ($query->have_posts()): $query->the_post();?>
        <li class="name">
            <a href="<?php the_permalink();?>" title="">
                <time><?php echo '* ' . get_the_date() . " - "; ?></time><?php the_title();?>
            </a>
            <?php the_content(); ?>
        </li>
    <?php endwhile;
    wp_pagenavi(['query' => $query]);
    wp_reset_postdata();
}

function news_post_list()
{
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $arg = [
        'post_type' => 'post',
        'posts_per_page' => 4,
        'paged' => $paged,
    ];
    $custom_query = new WP_Query($arg);

    while ($custom_query->have_posts()): $custom_query->the_post();?>

    <div class="row list-item">
        <div class="col-sm-3">
            <?php if (has_post_thumbnail()): ?>
                <strong><a href="<?php the_permalink();?>" title="<?php the_title_attribute();?>" >
                    <?php the_post_thumbnail();?>
                </a></strong>
            <?php endif;?>
            </div>
             <!-- end sm 3 -->
            <div class="col-sm-9">
                <a href="<?php esc_url(the_permalink())?>"><?php the_title('<h3>', '</h3>');?></a>
                <time><?php echo 'Ngày ' . get_the_date(); ?></time>
                <?php the_excerpt();?>
            </div>
        <!-- end sm 8 -->
        </div>

    <?php endwhile;
    wp_pagenavi(['query' => $custom_query]);
    wp_reset_postdata();
}

function get_post_from_cat($post_number, $cat)
{
    global $post;
    $arg = [
        'posts_per_page' => $post_number,
        'category_name' => $cat,
    ];
    $myposts = get_posts($arg);
    return $myposts;
}

function get_post_from_postype($post_number, $post_name){
    global $post;
    $arg = [
        'posts_per_page' => $post_number,
        'post_type' => $post_name,
    ];
    $myposts = get_posts($arg);
    return $myposts;
}


/*GET ID OF A PAGE*/
function get_ID_by_page_name($page_name) {
   global $wpdb;
   $page_name_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = '".$page_name."' AND post_type = 'page'");
   return $page_name_id;
}

/*BOOTSTRAP PAGINATION*/

add_filter('wp_pagenavi', 'bootstrap_navi', 10, 2);

function bootstrap_navi($html)
{
    $out = '';

    //wrap a's and span's in li's
    $out = str_replace("<div", "", $html);
    $out = str_replace("class='wp-pagenavi'>", "", $out);
    $out = str_replace("<a", "<li><a", $out);
    $out = str_replace("</a>", "</a></li>", $out);
    $out = str_replace("<span", "<li><span", $out);
    $out = str_replace("</span>", "</span></li>", $out);
    $out = str_replace("</div>", "", $out);

    return '<ul class="pagination">' . $out . '</ul>';
}

/*ADD VENDOR LINK*/
if (!function_exists('add_vendor_link')) {
    function add_vendor_link()
    {
        /*bootstrap css*/
        wp_register_style('bootstrap_stylesheet', "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css");
        wp_enqueue_style('bootstrap_stylesheet');

        /*style.css*/
        wp_register_style('main_stylesheet', get_template_directory_uri() . "/dist/css/app.min.css");
        wp_enqueue_style('main_stylesheet');

        /*Google font*/
        wp_register_style('google_font', "https://fonts.googleapis.com/css?family=PT+Sans");
        wp_enqueue_style('google_font');

        /*font awesome*/
        wp_register_style('fontwawesome', "//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css");
        wp_enqueue_style('fontwawesome');

        /*modernizr*/
        /* wp_register_script('modernizr', "//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js");
        wp_enqueue_script('modernizr');*/

        /*Jquery*/
        wp_enqueue_script('jquery');

        /*matchHeight js*/
        wp_register_script('matchHeight-js', "//cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.0/jquery.matchHeight-min.js", true);
        wp_enqueue_script('matchHeight-js');

        /*bootstrap js*/
        wp_register_script('bootstrap-js', "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js", true);
        wp_enqueue_script('bootstrap-js');

        /*main js*/
        wp_register_script('app-js', get_template_directory_uri() . "/dist/js/app.min-min.js", '', '', true);
        wp_enqueue_script('app-js');

        /*Script thanh toan dien tu*/
        wp_register_script('thanhtoan-js', "https://www.paybill.com.vn/themes/PayBill/script/iframe/FrameManager.js", '', '', true);
    }
}

add_action('wp_enqueue_scripts', 'add_vendor_link');

// =============== 22/05/2017 =======================
/**
 * Class support email via IMAP
 */
class Email_reader
{

    // imap server connection
    public $conn;

    // inbox storage and inbox message count
    private $inbox;
    private $msg_cnt;

    // email login credentials
    private $server;
    private $user;
    private $pass;
    private $port;

    // connect to the server and get the inbox emails
    public function __construct($server, $user, $pass, $port)
    {
        $this->server = $server;
        $this->user   = $user;
        $this->pass   = $pass;
        $this->port   = $port;
        $this->connect();
        $this->inbox();
    }

    // close the server connection
    public function close()
    {
        $this->inbox   = [];
        $this->msg_cnt = 0;

        imap_close($this->conn);
    }

    // open the server connection
    // the imap_open function parameters will need to be changed for the particular server
    // these are laid out to connect to a Dreamhost IMAP server
    public function connect()
    {
        $this->conn = imap_open('{' . $this->server . '/notls}', $this->user, $this->pass);
    }

    // move the message to a new folder
    public function move($msg_index, $folder = 'INBOX.Processed')
    {
        // move on server
        imap_mail_move($this->conn, $msg_index, $folder);
        imap_expunge($this->conn);

        // re-read the inbox
        $this->inbox();
    }

    // get a specific message (1 = first email, 2 = second email, etc.)
    public function get($msg_index = null)
    {
        if (count($this->inbox) <= 0) {
            return [];
        } elseif (!is_null($msg_index) && isset($this->inbox[$msg_index])) {
            return $this->inbox[$msg_index];
        }

        return $this->inbox[0];
    }

    // read the inbox
    public function inbox()
    {
        $this->msg_cnt = imap_num_msg($this->conn);

        $in = [];
        for ($i = 1; $i <= $this->msg_cnt; $i++) {
            $in[] = [
                'index'     => $i,
                'header'    => imap_headerinfo($this->conn, $i),
                'body'      => imap_body($this->conn, $i),
                'structure' => imap_fetchstructure($this->conn, $i),
            ];
        }

        $this->inbox = $in;
    }
}