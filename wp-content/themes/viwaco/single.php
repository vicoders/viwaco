<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package viwaco
 */

get_header();?>

	<div id="detail">
        <div class="container">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo home_url(); ?>">Trang chủ </a>
                </li>
                <li class="active"><?php wp_title('') ?></li>
            </ol>
        </div>
       <div class="container-fluid">
            <div class="row">
                <div class="col-sm-9" id="main-content">
                    <section id="news_detail">
	  <?php
while (have_posts()): the_post();

    get_template_part('template-parts/content', 'single');

endwhile; // End of the loop.
?>

					 </section>
                    <!-- end list -->
                </div>
                <!-- end sm9 -->

                <?php get_sidebar('chitiet');?>

            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end newlist -->
<?php
get_footer();
