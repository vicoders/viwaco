<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package viwaco
 */

if (!is_active_sidebar('sidebar-1')) {
    return;
}
?>

<aside class="col-sm-3 other-sidebar" id="intro-sidebar">
    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-stacked" id="sideMenu" role="tablist">
            <li class="submenu">
                Tổng quan về CTY CP VIWACO <span class="pull-right"><img src="<?php echo get_template_directory_uri() ?>/dist/img/gioithieu/plus.png" alt=""></span>
                <ul>
                    <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="" title="">Ban tổng hợp</a></li>
                    <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="" title="">Ban tổng hợp</a></li>
                    <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="" title="">Ban tổng hợp</a></li>
                </ul>
            </li>
            <li class="submenu">
                Tổ chức<span class="pull-right"><img src="<?php echo get_template_directory_uri() ?>/dist/img/gioithieu/plus.png" alt=""></span>
                <ul>
                    <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="" title="">Ban tổng hợp</a></li>
                    <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="" title="">Ban tổng hợp</a></li>
                    <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="" title="">Ban tổng hợp</a></li>
                </ul>
            </li>
            <li class="submenu">
                Nhân lực<span class="pull-right"><img src="<?php echo get_template_directory_uri() ?>/dist/img/gioithieu/plus.png" alt=""></span>
                <ul>
                    <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="" title="">Ban tổng hợp</a></li>
                    <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="" title="">Ban tổng hợp</a></li>
                    <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="" title="">Ban tổng hợp</a></li>
                </ul>
            </li>
            <li class="submenu">
                Lịch sử phát triển<span class="pull-right"><img src="<?php echo get_template_directory_uri() ?>/dist/img/gioithieu/plus.png" alt=""></span>
                <ul>
                    <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="" title="">Ban tổng hợp</a></li>
                    <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="" title="">Ban tổng hợp</a></li>
                    <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="" title="">Ban tổng hợp</a></li>
                </ul>
            </li>
            <li class="submenu">
                Báo cáo thường niên<span class="pull-right"><img src="<?php echo get_template_directory_uri() ?>/dist/img/gioithieu/plus.png" alt=""></span>
                <ul>
                    <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="" title="">Ban tổng hợp</a></li>
                    <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="" title="">Ban tổng hợp</a></li>
                    <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="" title="">Ban tổng hợp</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="hot-line">
        <div class="image">
            <img src="<?php echo get_template_directory_uri() ?>/dist/img/duong-day-nong.png" alt="duong-day-nong">
        </div>
    </div>
    <div class="service-item">
        <a href="" title="">
            <div class="row">
                <div class="col-xs-3">
                    <img src="<?php echo get_template_directory_uri() ?>/dist/img/header/payoo.png" alt="payoo">
                </div>
                <div class="col-xs-9 text-left">Thanh toán điện tử</div>
            </div>
        </a>
    </div>
    <div class="service-item">
        <a href="" title="">
            <div class="row">
                <div class="col-xs-3">
                    <img src="<?php echo get_template_directory_uri() ?>/dist/img/gioithieu/ersi.png" alt="ersi">
                </div>
                <div class="col-xs-9 text-left">Hệ thống quản lý GIS</div>
            </div>
        </a>
    </div>
    <div class="service-item">
        <a href="" title="">
            <div class="row">
                <div class="col-xs-3">
                    <img src="<?php echo get_template_directory_uri() ?>/dist/img/gioithieu/scada.png" alt="scada">
                </div>
                <div class="col-xs-9 text-left">Hệ thống SCADA</div>
            </div>
        </a>
    </div>
    <div class="service-item">
        <a href="" title="">
            <div class="row">
                <div class="col-xs-3">
                    <img src="<?php echo get_template_directory_uri() ?>/dist/img/gioithieu/noibo.png" alt="noibo">
                </div>
                <div class="col-xs-9 text-left">Nội bộ</div>
            </div>
        </a>
    </div>
    <div class="service-item">
        <a href="" title="">
            <div class="row">
                <div class="col-xs-3">
                    <img src="<?php echo get_template_directory_uri() ?>/dist/img/dichvu/file.png" alt="file">
                </div>
                <div class="col-xs-9 text-left">Thủ tục sang tên đồng hồ nước</div>
            </div>
        </a>
    </div>
    <div class="service-item">
        <a href="" title="">
            <div class="row">
                <div class="col-xs-3">
                    <img src="<?php echo get_template_directory_uri() ?>/dist/img/dichvu/contract.png" alt="t">
                </div>
                <div class="col-xs-9 text-left">Đăng ký gắn đồng hồ mới</div>
            </div>
        </a>
    </div>
    <!-- end sm4 -->
    <div class="service-item">
        <a href="" title="">
            <div class="row">
                <div class="col-xs-3">
                    <img src="<?php echo get_template_directory_uri() ?>/dist/img/dichvu/thanh-toan.png" alt="t">
                </div>
                <div class="col-xs-9 text-left">Hình thức thanh toán tiền nước</div>
            </div>
        </a>
    </div>
    <!-- end sm4 -->
    <div class="service-item">
        <a href="" title="">
            <div class="row">
                <div class="col-xs-3">
                    <img src="<?php echo get_template_directory_uri() ?>/dist/img/dichvu/huong-dan.png" alt="folder">
                </div>
                <div class="col-xs-9 text-left">Hướng dẫn đăng ký định mức nước</div>
            </div>
        </a>
    </div>
    <!-- end sm4 -->
    <div class="service-item">
        <a href="" title="">
            <div class="row">
                <div class="col-xs-3">
                    <img src="<?php echo get_template_directory_uri() ?>/dist/img/dichvu/bieu-gia.png" alt="bieugia">
                </div>
                <div class="col-xs-9 text-left">Giá và biểu giá tính tiền nước</div>
            </div>
        </a>
    </div>
    <!-- end sm4 -->
    <div class="service-item">
        <a href="" title="">
            <div class="row">
                <div class="col-xs-3">
                    <img src="<?php echo get_template_directory_uri() ?>/dist/img/dichvu/thay-dong-ho.png" alt="thaydongho">
                </div>
                <div class="col-xs-9 text-left">Thay đồng hồ nước</div>
            </div>
        </a>
    </div>
    <!-- end sm4 -->
    <!-- end payment -->
    <div id="map" class="sidebar-style">
        <h2>BẢN ĐỒ VỊ TRÍ</h2>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6481209470253!2d105.8020842143078!3d21.00673779390705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135aca1f27d1b5d%3A0x51c5f72058d51810!2zQ8O0bmcgVHkgQ-G7lSBQaOG6p24gVklXQUNP!5e0!3m2!1svi!2s!4v1490346344332" width="100%"  frameborder="0" style="border:0" allowfullscreen></iframe>
    </div><!-- end map -->
    <div id="gis" class="sidebar-style">
        <h2>HỆ THỐNG GIS</h2>
        <div style="overflow:hidden">
            <img src="<?php echo get_template_directory_uri() ?>/dist/img/gis.png" alt="">
        </div>

    </div>
    <!-- end gis -->
    <div id="multimeter" class="sidebar-style">
        <h2>ĐỒNG HỒ ĐIỆN TỬ</h2>
        <div style="overflow:hidden">
            <img src="<?php echo get_template_directory_uri() ?>/dist/img/dong-ho.png" alt="">
        </div>
    </div>
    <!-- end multimeter -->
</aside>
<!-- end sm3 -->

