var gulp = require('gulp');
var sass = require('gulp-sass');
var sassGlob = require('gulp-sass-glob');
var watch = require('gulp-watch'); //watching file
var concat = require('gulp-concat'); //concatenating file
var sourcemaps = require('gulp-sourcemaps'); //show the source of scss rules
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var plumber = require('gulp-plumber'); //notify gulp when error occurs
var cleanCss = require('gulp-clean-css'); // minify CSS
var minify = require('gulp-minify'); // minify JS

// browser-sync task for starting the server.
gulp.task('browser-sync', function() {
    //watch files
    var files = [
        './**/.css',
        './*.php'
    ];

    //initialize browsersync
    browserSync.init(files, {
        //browsersync with a php server
        proxy: "vicoder.dev",
        notify: false
    });
});


gulp.task('sass', function() {
    return gulp.src('assets/scss/*.scss')
        .pipe(plumber(function(error) {
            console.log(error.toString());
            this.emit('end');
        }))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sassGlob())
        .pipe(autoprefixer())
        .pipe(concat('app.min.css'))
        .pipe(cleanCss())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/css/'))
    browserSync.reload({ stream: true });
});

gulp.task('js', function() {
    return gulp.src('assets/js/*.js')
        .pipe(plumber(function(error) {
            console.log(error.toString());
            this.emit('end');
        }))
        .pipe(concat('app.min.js'))
        .pipe(minify())
        .pipe(gulp.dest('dist/js/'))
    browserSync.reload({ stream: true });
});



gulp.task('watch', function() {
    gulp.watch('assets/scss/*.scss', ['sass']);
    gulp.watch('assets/js/*.js', ['js']);
});

gulp.task('default', ['sass', 'js', 'watch']);
