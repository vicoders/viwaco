<?php
/*Template Name: Home Page*/
;?>
<?php get_header();?>
<?php layerslider(1);?>

    <!--==========================
        =            Main            =
        ===========================-->
    <div id="home" class="container-fluid">
        <!--====  Outstanding  ====-->
        <div class="row">
            <section id="outstanding" class="col-sm-6">
                <h2 class="heading-section">Nổi bật</h2>
                <?php dynamic_sidebar('sidebar-3');?>
            </section>
            <section id="news" class="col-sm-3">
                <h2 class="heading-section">Điểm tin</h2>
            </section>
            <div class="hot-line col-sm-3">
                <h2 class="text-center">Đường dây nóng</h2>
                <div class="image">
                    <img src="<?php echo get_template_directory_uri() ?>/dist/img/duong-day-nong.png" alt="duong-day-nong">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-9" id="main-content">
                <!--====  SERVICE  ====-->
                <section id="service">
                    <h2><span><img src="<?php echo get_template_directory_uri(); ?>/dist/img/dichvu/service-icon.png" alt=""></span>Dịch vụ khách hàng</h2>
                    <div class="service-list">
                        <div class="row">
                            <div class="col-sm-6 col-lg-4 col-xs-12 service-item">
                                <div class="col-sm-12">
                                    <a href="" title="">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/dichvu/file.png" alt="file">
                                            </div>
                                            <div class="col-xs-9 text-left">Thủ tục sang tên đồng hồ nước</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- end sm4 -->
                            <div class="col-sm-6 col-lg-4 col-xs-12 service-item">
                                <div class="col-sm-12">
                                    <a href="" title="">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/dichvu/contract.png" alt="t">
                                            </div>
                                            <div class="col-xs-9 text-left">Đăng ký gắn đồng hồ mới</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- end sm4 -->
                            <div class="col-sm-6 col-lg-4 col-xs-12 service-item">
                                <div class="col-sm-12">
                                    <a href="" title="">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/dichvu/thanh-toan.png" alt="t">
                                            </div>
                                            <div class="col-xs-9 text-left">Hình thức thanh toán tiền nước</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- end sm4 -->
                            <div class="col-sm-6 col-lg-4 col-xs-12 service-item">
                                <div class="col-sm-12">
                                    <a href="" title="">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/dichvu/huong-dan.png" alt="folder">
                                            </div>
                                            <div class="col-xs-9 text-left">Hướng dẫn đăng ký định mức nước</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- end sm4 -->
                            <div class="col-sm-6 col-lg-4 col-xs-12 service-item">
                                <div class="col-sm-12">
                                    <a href="" title="">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/dichvu/bieu-gia.png" alt="bieugia">
                                            </div>
                                            <div class="col-xs-9 text-left">Giá và biểu giá tính tiền nước</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- end sm4 -->
                            <div class="col-sm-6 col-lg-4 col-xs-12 service-item">
                                <div class="col-sm-12">
                                    <a href="" title="">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <img src="<?php echo get_template_directory_uri(); ?>/dist/img/dichvu/thay-dong-ho.png" alt="thaydongho">
                                            </div>
                                            <div class="col-xs-9 text-left">Thay đồng hồ nước</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!-- end sm4 -->
                        </div>
                    </div>
                    <!-- end service list -->

                    <div class="department">
                        <div class="row">
                            <div class="col-sm-4">
                                <?php echo do_shortcode('[slick-slider design="design-1" category="11" sliderheight="115" dots="false" arrows="true" autoplay="true" autoplay_interval="5000" speed="1000"]'); ?>
                            </div>
                            <!-- end sm4 -->
                            <div class="col-sm-8">
                                <ul>
                                    <div class="row">
                                        <?php 
                                        $myposts = get_post_from_postype(-1,'ban_nganh');
                                        foreach ($myposts as $post):?>
                                            <li class="col-sm-6">
                                                <i class="fa fa-check-square-o" aria-hidden="true"></i><a href="<?php the_permalink(); ?>" title="<?php the_title()?>"><?php the_title( ); ?></a> 
                                            </li>
                                        <?php endforeach; ?>
                                    </div>
                                </ul>
                                <!-- end ul -->
                            </div>
                            <!-- end sm8 -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end department -->

                </section>
                <!-- end service -->

                <div class="row">
                    <div class="seminar col-sm-6">
                        <h2>Chuyên đề</h2>
                        <ul>
                            <?php 
                                $myposts = get_post_from_cat(4, 'chuyen_de');
                                if ($myposts) {
                                    foreach ($myposts as $post):
                                        setup_postdata($post);?>
                                            <li class="most-viewed-item clearfix">
                                                <a href="<?php the_permalink();?>">
                                                    <?php if (has_post_thumbnail()) {
                                                        the_post_thumbnail(array(100,60));
                                                    } ?>
                                                </a>

                                                <a href="<?php the_permalink();?>"><?php the_title()?></a>
                                            </li><?php
                                    endforeach;
                                    wp_reset_postdata();
                                }
                            ?>
                        </ul>
                    </div>
                    <!-- end seminar -->

                    <div class="project col-sm-6">
                        <h2>Dự án</h2>
                        <ul>
                            <?php 
                                $myposts = get_post_from_cat(4, 'du_an');
                                if ($myposts) {
                                    foreach ($myposts as $post):
                                        setup_postdata($post);?>
                                            <li class="most-viewed-item clearfix">
                                                <a href="<?php the_permalink();?>">
                                                    <?php if (has_post_thumbnail()) {
                                                        the_post_thumbnail(array(100,60));
                                                    } ?>
                                                </a>

                                                <a href="<?php the_permalink();?>"><?php the_title()?></a>
                                            </li><?php
                                    endforeach;
                                    wp_reset_postdata();
                                }
                            ?>
                        </ul>
                    </div>
                    <!-- end project -->

                </div>

                <!--====  Saving banner  ====-->
                <div id="saving">

                <?php layerslider(5);?>
                </div>
                <!-- end saving banner -->
                <section id="related">
                    <div class="panel">
                        <div class="panel-heading">
                            <h2 class="panel-title"><span><img src="<?php echo get_template_directory_uri(); ?>/dist/img/lienquan/icon.png" alt="relate"></span>Nước sạch và các vấn đề liên quan</h2>
                            <ul class="nav panel-tabs">
                                <li class="active"><a href="#tab1" data-toggle="tab">VIWACO</a></li>
                                <li><a href="#tab2" data-toggle="tab">Hà Nội</a></li>
                                <li><a href="#tab3" data-toggle="tab">Khoa học</a></li>
                                <li><a href="#tab4" data-toggle="tab">Cuộc sống</a></li>
                                <li><a href="#tab5" data-toggle="tab">Vi phạm</a></li>
                                <li><a href="#tab6" data-toggle="tab">Video</a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">

                                <div class="tab-pane active" id="tab1">
                                    <ul class="no-reload-1">
                                        <?php relating_post_list(4,'');?>
                                    </ul>
                                </div>
                                <div class="tab-pane" id="tab2">
                                    <ul class="no-reload-2">
                                        <?php relating_post_list(4,'chuyen_de');?>
                                    </ul>
                                </div>
                                <div class="tab-pane" id="tab3">
                                    <ul class="no-reload-3">
                                        <?php relating_post_list(4,'du_an');?>
                                    </ul>
                                </div>
                                <div class="tab-pane" id="tab4">
                                    <ul class="no-reload-4">
                                        <?php relating_post_list(4,'');?>
                                    </ul>
                                </div>

                                 <div class="tab-pane" id="tab5">
                                    <ul class="no-reload-5">
                                        <?php relating_post_list(4,'');?>
                                    </ul>
                                </div>

                                 <div class="tab-pane" id="tab6">
                                    <ul class="no-reload-6">
                                        <?php relating_post_list_video(1);?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- end related -->
            </div>
            <!-- end sm9 -->
            <?php get_sidebar('home');?>
        </div>
        <!-- end row -->
    </div>
    <!-- end home -->

    <!--====  /Main  ====-->
    <?php get_footer();?>
</body>

</html>
