<?php
/**
 * Template Name: Văn bản pháp quy
 */
get_header();

$category = get_the_category(); 
$cat_id = $category[0]->cat_ID; 

$paged = ($_GET['trang'] >= 2) ? $_GET['trang'] : 1;
$arg = [
    'post_type' => 'post',
    'posts_per_page' => 4,
    'paged' => $paged,
    'cat' => $cat_id,
];
$custom_query = new WP_Query($arg);
$total_pages = $custom_query->max_num_pages;
// echo "<pre>";
// var_dump($category); die;
?>
    <div id="news_list">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-9" id="main-content">
                    <section id="list">
                        <h2><?php echo get_cat_name( $cat_id ); ?> </h2>
                        <?php							
                            while ($custom_query->have_posts()): $custom_query->the_post();?>
                            <div class="row list-item">
                                <div class="col-sm-3">
                                    <?php if (has_post_thumbnail()): ?>
                                        <strong><a href="<?php the_permalink();?>" title="<?php the_title_attribute();?>" >
                                            <?php the_post_thumbnail();?>
                                        </a></strong>
                                    <?php endif;?>
                                    </div>
                                    <div class="col-sm-9">
                                        <a href="<?php esc_url(the_permalink())?>"><?php the_title('<h3>', '</h3>');?></a>
                                        <time><?php echo 'Ngày ' . get_the_date(); ?></time>
                                        <?php the_excerpt();?>
                                    </div>
                                </div>
                            <style type="text/css">
                                .paginate {
                                    margin-top: 10px;
                                }
                                .paginate .page-numbers {
                                    position: relative;
                                    float: left;
                                    padding: 6px 12px;
                                    margin-left: -1px;
                                    line-height: 1.42857143;
                                    color: #337ab7;
                                    text-decoration: none;
                                    background-color: #fff;
                                    border: 1px solid #ddd;
                                }
                                .paginate .current {
                                    background: #07418E;
                                    color: #fff;
                                }
                            </style>
                            <?php endwhile;
                            echo "<div class='paginate pull-right'>";
                            if ($total_pages > 1){
                                $current_page = max(1, $paged);
                         
                                echo paginate_links(array(
                                    'base' => @add_query_arg('trang','%#%'),
                                    'format' => '?trang=%#%',
                                    'current' => $current_page,
                                    'total' => $total_pages,
                                ));
                            }
                            echo "</div>";
                            wp_reset_postdata();
                        ?>	                    
                    </section>
	                </div>
	                <?php get_sidebar('other');?>
	            </div>
	        </div>
	    </div>
<?php get_footer();?>
