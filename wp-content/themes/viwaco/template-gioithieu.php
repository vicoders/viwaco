<?php
/*Template Name: Introduction Page*/
get_header();
?>

<!--==========================
    =            Main            =
    ===========================-->
    <div id="intro">
        <div class="container">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo home_url(); ?>">Trang chủ </a>
                </li>
                <li class="active"><?php the_title()?></li>
            </ol>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-9" id="main-content">
                    <!--====  SERVICE  ====-->
                    <section id="service">
                        <h2><span><img src="<?php echo get_template_directory_uri() ?>/dist/img/dichvu/service-icon.png" alt=""></span>DỊCH VỤ KHÁCH HÀNG</h2>
                        
                        <div class="department">
                            <div class="row">
                                <div class="col-sm-4">
									 <?php echo do_shortcode('[slick-slider design="design-1" category="11" show_content="true" dots="false" arrows="true" autoplay="true" autoplay_interval="5000" speed="1000" sliderheight="115"]'); ?>
                                </div>
                                <!-- end sm4 -->
                                <div class="col-sm-8">
                                    <ul>
                                        <div class="row">
                                            <?php 
                                            $myposts = get_post_from_postype(-1,'ban_nganh');
                                            foreach ($myposts as $post):?>
                                                <li class="col-sm-6">
                                                    <i class="fa fa-check-square-o" aria-hidden="true"></i><a href="<?php the_permalink(); ?>" title="<?php the_title()?>"><?php the_title( ); ?></a> 
                                                </li>
                                            <?php endforeach; ?>
                                        </div>
                                    </ul>
                                    <!-- end ul -->
                                </div>
                                <!-- end sm8 -->
                            </div>
                        </div>
                        <!-- end row -->
                    </section>
                    <!-- end service -->
                    <!--====  Company Intro  ====-->
                    <section id="company-intro">
                        <h2><span><img src="<?php echo get_template_directory_uri() ?>/dist/img/lienquan/icon.png" alt=""></span>GIỚI THIỆU CÔNG TY</h2>
                        <div class="company-intro-body">
                            <div class="company-info">
                                <ul>
                                    <li>Tên công ty : CÔNG TY CỔ PHẦN VIWACO</li>
                                    <li>Tên giao dịch : VIWACO JOINT STOCK COMPANY.</li>
                                    <li>Tên viết tắt : VIWACO., JSC</li>
                                    <li>Địa chỉ trụ sở chính : Tầng 1, nhà 17 – T7 Khu đô thị Trung Hoà – Nhân Chính, Phường Nhân chính, Quận Thanh Xuân, Hà Nội.</li>
                                    <li>Điện thoại : (84.4) 62511520, 62511523 Fax : (84.4) 62511524</li>
                                    <li>Website : http://viwaco.vn Email : info@viwaco.vn</li>
                                </ul>
                            </div>
                            <p>
                                Công ty cổ phần VIWACO được thành lập vào ngày 17/3/2005 ban đầu gồm 3 cổ đông sáng lập là Tổng Công ty xuất nhập khẩu xây dựng Việt Nam VINACONEX, Công ty kinh doanh nước sạch Hà Nội và Công ty cổ phần VIGLAFICO với nhiệm vụ đầu tư mạng lưới cấp nước khu vực Tây nam Thành phố Hà Nội để tiếp nhận và cung cấp nguồn nước sạch Sông Đà cấp nước tới toàn bộ nhân dân khu vực Tây nam Hà Nội bao gồm toàn bộ Quận Thanh Xuân, một phần phường Trung Hòa, Mai Dịch quận Cầu Giấy, phường Định Công, Đại Kim, Thịnh Liệt quận Hoàng mai, quận Nam Từ Liêm và các xã phía Tây quốc lộ 1A huyện Thanh Trì Hà Nội.
                            </p>
                            <p>
                                Ngay từ ngày mới thành lập với mô hình là một Doanh nghiệp cổ phần hoạt động kinh doanh lĩnh vực cung cấp nước sạch, giá bán nước sạch theo quy định của Nhà nước. Mọi thứ còn mới mẻ, cơ chế chính sách của Nhà nước đối với Doanh nghiệp cổ phần tham gia sản xuất và cung cấp nước chưa tạo ra được nhiều điểm tựa vững chắc để Doanh nghiệp có đủ điều kiện mở rộng đầu tư mạng lưới cấp nước, nâng cao dịch vụ cấp nước cho khách hàng. Công tác huy động nguồn vốn gặp rất nhiều khó khăn, hiệu quả đầu tư các dự án cấp nước thường mang lại hiệu quả xã hội là chính, trong khi các tổ chức tín dụng lại quan tâm đến hiệu quả kinh tế, tỷ suất lợi nhuận, khả năng sinh lời, trả nợ…
                            </p>
                            <p>
                                Mặc dù gặp rất nhiều khó khăn nhưng dưới sự chỉ đạo, hỗ trợ của UBND Thành phố Hà Nội, các Sở Ban ngành Thành phố, Tổng Công ty CP Xuất nhập khẩu và xây dựng Việt Nam – Vinaconex, Công ty VIWACO đã và đang từng bước vượt qua khó khăn và tiến lên phát triển ổn định và bền vững, góp phần không nhỏ vào việc nâng cao chất lượng cuộc sống của nhân dân khu vực Tây nam Thành phố Hà nội.
                            </p>
                            <p>
                                Qua 10 năm xây dựng và phát triển đến nay, mạng lưới cấp nước của Công ty đã phủ kín toàn bộ quận Thanh Xuân, quận Nam Từ Liêm, phường Định Công, Đại Kim, Thịnh Liệt quận Hoàng Mai và một số xã huyện Thanh Trì đảm bảo cung cấp nước sạch Sông Đàcho 100% các hộ dân đạt chất lượng,ổn định liên tục 24/24h. Kể từ tháng 4 năm 2009 thời điểm bắt đầu tiếp nhận nguồn nước mặt Sông Đà công suất cấp chỉ 70.000m3/ngđ đến nay đã đạt trên 170.000m3/ngđ.Số lượng khách hàng được sử dụng nguồn nước sạch Sông Đà cũng tăng từ 50.000 khách hàng năm 2009 lên trên120.000 khách hàng trong năm 2014.
                            </p>
                            <p>
                                Mục tiêu giai đoạn 2015-2018 hoàn thành việc đầu tư mạng lưới cấp nước cho các khu vực còn lại của phường Phương Canh, Xuân Phương quận Nam Từ Liêm vàcác xã còn lại của huyện Thanh Trìnằm trong địa bàn Công ty VIWACO quản lý.Phấn đấu nâng công suất cấp nước đạt 300.000m3/ngđ và khách hàng đạt 180.000 khách hàng vào năm 2018.
                            </p>
                            LĨNH VỰC HOẠT ĐỘNG
                            <ul>
                                <li>- Sản xuất và cung cấp nước sạch.</li>
                                <li>- Tư vấn đầu tư xây dựng các công trình chuyên ngành cấp thoát nước.</li>
                                <li>- Thi công xây lắp các công trình dân dụng, công nghiệp, cấp thoát nước, xử lý chất thải, các công trình điện, đường dây trạm biến thế đến 35KV.</li>
                                <li>- Sản xuất và kinh doanh máy móc, thiết bị, vật tư ngành nước.</li>
                                <li>- Đầu tư và kinh doanh bất động sản.</li>
                                <li>- Thiết kế hệ thống cấp thoát nước khu đô thị và nông thôn, xử lý nước thải và nước sinh hoạt.</li>
                                <li>- Thiết kế cấp thoát nước: Đối với các công trình xây dựng dân dụng, công nghiệp, môi trường nước.</li>
                                <li>- Thiết kế tổng mặt bằng, kiến trúc, nội ngoại thất: Đối với công trình xây dựng dân dụng, công nghiệp.</li>
                                <li>- Tư vấn giám sát chất lượng xây dựng. - Khảo sát xây dựng.</li>
                            </ul>
                            <img class="text-center" src="<?php echo get_template_directory_uri() ?>/dist/img/gioithieu/so-do-cty.png" alt="Sơ đồ công ty">
                        </div>
                        <!-- end company intro body -->
                    </section>
                    <!-- end company intro -->
                </div>
                <!-- end sm9 -->
                <?php get_sidebar('gioithieu');?>
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end intro -->
<?php get_footer();?>