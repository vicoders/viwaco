<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package viwaco
 */

?><!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
<meta charset="<?php bloginfo('charset');?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head();?>
</head>

<body <?php body_class();?>>
	<!--============================
    =            Header            =
    =============================-->
    <div class="contact">
        <ul class="list-inline">
            <li><span><img src="<?php echo get_template_directory_uri() ?>/dist/img/header/phone.png" alt="phone"></span>0981.923.715</li>
            <li><span><img src="<?php echo get_template_directory_uri() ?>/dist/img/header/mail.png" alt="mail"></span>info@viwaco.vn </li>
        </ul>
    </div>
    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 logo">
                    <a href="<?php echo home_url(); ?>" title=""><img src="<?php echo get_template_directory_uri() ?>/dist/img/header/logo.png" alt="site-logo"></a>
                    <!-- end site logo -->
                </div>
                <div class="col-md-6 list-icon">
                    
                    <ul>
                        <li>
                            <a href="<?php echo get_page_uri(get_ID_by_page_name('thanh-toan-dien-tu'))?>" title="Payoo">
                                <img src="<?php echo get_template_directory_uri() ?>/dist/img/header/payoo.png" alt="payoo">
                            </a>
                        </li>

                        <li>
                            <a href="" title="Ersi">
                                <img src="<?php echo get_template_directory_uri() ?>/dist/img/header/ersi.png" alt="ersi">
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo get_page_uri(get_ID_by_page_name('dong-ho-dien-tu')) ?>" title="Đồng hồ điện tử">
                                <img src="<?php echo get_template_directory_uri() ?>/dist/img/header/scada.png" alt="Đồng hồ điện tử">
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo get_page_uri(get_ID_by_page_name('noi-bo')) ?>" title="Webmail">
                                <img src="<?php echo get_template_directory_uri() ?>/dist/img/header/noibo.png" alt="noibo">
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo wp_login_url(); ?>" title="Admin">
                                <img src="<?php echo get_template_directory_uri() ?>/dist/img/header/admin.png" alt="login">
                            </a>
                        </li>

                       <!--  <li><a href="<?php echo get_page_uri(get_ID_by_page_name('bom-tang-ap')); ?>" title="Bơm tăng áp"><img src="<?php echo get_template_directory_uri() ?>/dist/img/header/turbocharger.png" alt="login"></a></li> -->
                    </ul>
                    <form class="navbar-form pull-right" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Tìm kiếm">
                            <img src="<?php echo get_template_directory_uri() ?>/dist/img/header/search.png" alt="">
                        </div>
                    </form>
                </div>
                <!-- end contact -->
            </div>
        </div>

        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <div class="hidden-md hidden-lg">Menu</div>
                    <button type="button" class="navbar-toggle" >
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse navbar-ex1-collapse">
                <?php main_menu();?>
	                <!-- <form class="navbar-form navbar-right hidden-sm hidden-xs" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Tìm kiếm">
                            <img src="<?php echo get_template_directory_uri() ?>/dist/img/header/search.png" alt="">
                        </div>
                    </form> -->
              	</div>
                <!-- /.navbar-collapse -->
            </div>
        </nav>
    </header>
    <!-- /header -->
