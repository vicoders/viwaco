<?php the_title('<h3>', '</h3>')?>
<time><?php echo get_current_weekday() ?></time>
<div class="fb-like" data-href="<?php the_permalink();?>" data-width="400" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
<?php the_content();?>
