
<?php

get_header();
?>

<!--==========================
    =            Main            =
    ===========================-->
    <div id="intro">
        <div class="container">
            <ol class="breadcrumb">
                <li>
                    <a href="<?php echo home_url(); ?>">Trang chủ </a>
                </li>
                <li class="active"><?php the_title()?></li>
            </ol>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-9" id="main-content">
					<?php if (have_posts()): while (have_posts()): the_post();
        get_template_part('template-parts/content', get_post_format());
    endwhile;
else:
    get_template_part('template-parts/content', 'none');
endif;?>
                </div>
                <!-- end sm9 -->
                <?php get_sidebar('gioithieu');?>
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end intro -->
<?php get_footer();?>

