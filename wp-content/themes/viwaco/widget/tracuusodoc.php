<?php
/**
 * Class create widget get newest post
 */
class WidgetNewestPost extends WP_Widget
{
    public function __construct()
    {
        parent::__construct(
            'tracuusodoc',
            'Tra cứu sổ đọc',
            ['description' => 'Tra cứu sổ đọc']
        );
        add_action('widgets_init', [$this, 'create_newestpost_widget']);
    }

    public function form($instance)
    {
        $default = [
            'title'    => '',
            'user'     => '',
            'password' => '',
            'host'     => '',
            'port'     => '',
        ];
        $instance = wp_parse_args((array) $instance, $default);
        $title    = esc_attr($instance['title']);
        $user     = esc_attr($instance['user']);
        $password = esc_attr($instance['password']);
        $host     = esc_attr($instance['host']);
        $port     = esc_attr($instance['port']);

        echo '<p>Nhập tiêu đề <input type="text" class="widefat" name="' . $this->get_field_name('title') . '" value="' . $title . '"/></p>';
        echo '<p>Host: <input type="text" class="widefat form-control" name="' . $this->get_field_name('host') . '" value="' . $host . '" placeholder="' . $host . '" /></p>';
        echo '<p>Port: <input type="text" class="widefat form-control" name="' . $this->get_field_name('port') . '" value="' . $port . '" placeholder="' . $port . '" /></p>';
        // echo '<p>Username <input type="text" class="widefat" name="' . $this->get_field_name('user') . '" value="' . $user . '"/></p>';
        // echo '<p>Password <input type="password" class="widefat form-control" name="' . $this->get_field_name('password') . '" value="' . $password . '" placeholder="' . $password . '" max="30" /></p>';
    }

    public function update($new_instance, $old_instance)
    {
        $instance             = $old_instance;
        $instance['title']    = strip_tags($new_instance['title']);
        // $instance['user']     = strip_tags($new_instance['user']);
        // $instance['password'] = strip_tags($new_instance['password']);
        $instance['host']     = strip_tags($new_instance['host']);
        $instance['port']     = strip_tags($new_instance['port']);
        return $instance;
    }

    public function widget($args, $instance)
    {
        global $post;
        extract($args);
        $title    = apply_filters('widget_title', $instance['title']);
        // $username = $instance['user'];
        // $password = $instance['password'];
        $host     = $instance['host'];
        $port     = $instance['port'];

        echo $before_widget;
        ?>
        <style type="text/css">
        	.tracuusodoc {
        		background: #2196f3;
        		margin-bottom: 15px;
        	}
        	.tracuusodoc form #submit-btn{
        		background: #2196f3;
			    color: #fff;
			    border: 1px solid #fff;
			    box-shadow: none;
			    border-radius: 0;
        	}

        	.tracuusodoc form #submit-btn:hover{
        		background: #fff;
    			color: #2196f3;
        	}

        	.tracuusodoc .form-area label {
    		    color: #fff;
    		    font-weight: 200;
        	}
        	.tracuusodoc .form-area .btn-container{
        		margin-bottom: 15px;
        	}
        	.tracuusodoc .form-area h3{
        		margin-bottom: 10px;
        		text-align: center;
        		color: #fff;
        		text-transform: uppercase;
    		    font-size: 20px;
        	}
        	.tracuusodoc .form-area .form-group {
        		padding-left: 20px;
    			padding-right: 20px;
        	}
        </style>
        <div class="tracuusodoc">
	        <div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
				    <div class="form-area">
				        <form role="form" method="post" action="<?php echo site_url('tra-cuu-so-doc'); ?>">
		                    <h3><?php echo $title; ?></h3>
		    				<div class="form-group">
		    					<label for="inp_username">Username</label>
								<input type="text" class="form-control" id="inp_username" name="inp_username" placeholder="Username" required>
							</div>
							<div class="form-group">
								<label for="inp_password">Password</label>
								<input type="password" class="form-control" id="inp_password" name="inp_password" placeholder="Password" required>
							</div>
							<div class="form-group">
								<label for="makh">Mã khách hàng</label>
								<input type="text" class="form-control" id="makh" name="makh" placeholder="Mã Khách hàng">
							</div>
							<div class="row text-center btn-container">
                                <input type="hidden" name="action" value="tracuusodoc_action">
                                <input type="hidden" name="host" value="<?php echo $host; ?>">
                                <input type="hidden" name="port" value="<?php echo $port; ?>">
				        		<button type="submit" id="submit-btn" name="submit" class="btn btn-primary">TRA CỨU</button>
				        	</div>
				        </form>
				    </div>
				</div>
			</div>
		</div>

        <?php

        echo $after_widget;
    }

    public function create_newestpost_widget()
    {
        register_widget('WidgetNewestPost');
    }
}

new WidgetNewestPost();

