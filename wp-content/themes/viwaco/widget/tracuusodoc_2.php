<?php
/**
 * Class create widget get newest post
 */
class WidgetNewestPost extends WP_Widget
{
    public function __construct()
    {
        parent::__construct(
            'tracuusodoc',
            'Tra cứu sổ đọc',
            ['description' => 'Tra cứu sổ đọc']
        );
        add_action('widgets_init', [$this, 'create_newestpost_widget']);
    }

    public function form($instance)
    {
        $default = [
            'title'    => '',
            'user'     => '',
            'password' => '',
            'host'     => '',
            'port'     => '',
        ];
        $instance = wp_parse_args((array) $instance, $default);
        $title    = esc_attr($instance['title']);
        $user     = esc_attr($instance['user']);
        $password = esc_attr($instance['password']);
        $host     = esc_attr($instance['host']);
        $port     = esc_attr($instance['port']);

        echo '<p>Nhập tiêu đề <input type="text" class="widefat" name="' . $this->get_field_name('title') . '" value="' . $title . '"/></p>';
        echo '<p>Host: <input type="text" class="widefat form-control" name="' . $this->get_field_name('host') . '" value="' . $host . '" placeholder="' . $host . '" /></p>';
        echo '<p>Port: <input type="text" class="widefat form-control" name="' . $this->get_field_name('port') . '" value="' . $port . '" placeholder="' . $port . '" /></p>';
        echo '<p>Username <input type="text" class="widefat" name="' . $this->get_field_name('user') . '" value="' . $user . '"/></p>';
        echo '<p>Password <input type="password" class="widefat form-control" name="' . $this->get_field_name('password') . '" value="' . $password . '" placeholder="' . $password . '" max="30" /></p>';
    }

    public function update($new_instance, $old_instance)
    {
        $instance             = $old_instance;
        $instance['title']    = strip_tags($new_instance['title']);
        $instance['user']     = strip_tags($new_instance['user']);
        $instance['password'] = strip_tags($new_instance['password']);
        $instance['host']     = strip_tags($new_instance['host']);
        $instance['port']     = strip_tags($new_instance['port']);
        return $instance;
    }

    public function widget($args, $instance)
    {
        global $post;
        extract($args);
        $title    = apply_filters('widget_title', $instance['title']);
        $user     = ($instance['user']);
        $password = ($instance['password']);
        $host     = ($instance['host']);
        $port     = esc_attr($instance['port']);

        echo $before_widget;

        // User : test
        // Pass : Vmms1234#
        // IMAP Server: mail.viwaco.vn  ; Port: 143
        // SMTP Server: mail.viwaco.vn ; Port: 587

        // require_once get_template_directory() . '/widget/Net_SMTP/Net/SMTP.php';
        // require_once get_template_directory() . '/widget/mail/Mail.php';

  //       $from = 'vicoders@mail.com';
		// $to = 'daudq.info@mail.com'; // change to address
		// $subject = 'Hello'; // subject of mail
		// $body = "Hello world! this is the content of the email"; //content of mail

		// $headers = array(
		//     'From' => $from,
		//     'To' => $to,
		//     'Subject' => $subject
		// );

  //       $smtp = Mail::factory('smtp', [
  //           'host'     => 'mail.viwaco.vn',
  //           'port'     => '143',
  //           'auth'     => true,
  //           'username' => 'test',
  //           'password' => 'Vmms1234#',
  //       ]);
  //       $mail = $smtp->send($to, $headers, $body);

        // require_once get_template_directory() . '/widget/lib/TurboApiClient.php';
        // $res_api = new TurboApiClient('test', 'Vmms1234#');
        // 
        
        require_once get_template_directory() . '/widget/PHPMailer/PHPMailerAutoload.php';
        $mail = new PHPMailer;
        $mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'ssl://mail.viwaco.vn';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'test@viwaco.vn';                 // SMTP username
		$mail->Password = 'Vmms1234#';                           // SMTP password
		$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to


		// $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
		// $mail->SMTPAuth = true;                               // Enable SMTP authentication
		// $mail->Username = 'vicoders.daily.report@gmail.com';                 // SMTP username
		// $mail->Password = 'davwohsqrwbevcwd';                           // SMTP password
		// $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		// $mail->Port = 587;  

		// $mail->setFrom('test@viwaco.vn', 'Mailer');
		// $mail->addReplyTo('daudq.info@gmail.com', 'Information');
		// $mail->addCC('daudq.info@gmail.com');
		// $mail->addBCC('bcc@example.com');

		// $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		// $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
		// $mail->isHTML(true);                                  // Set email format to HTML

		// $mail->Subject = 'Here is the subject';
		// $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
		// $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $res_mail = $mail->smtpConnect();

        echo "<pre>";
        var_dump($res_mail);die;
        ?>
        <style type="text/css">
        	.tracuusodoc {
        		background: #2196f3;
        		margin-bottom: 15px;
        	}
        	.tracuusodoc form #submit-btn{
        		background: #2196f3;
			    color: #fff;
			    border: 1px solid #fff;
			    box-shadow: none;
			    border-radius: 0;
        	}

        	.tracuusodoc form #submit-btn:hover{
        		background: #fff;
    			color: #2196f3;
        	}

        	.tracuusodoc .form-area label {
    		    color: #fff;
    		    font-weight: 200;
        	}
        	.tracuusodoc .form-area .btn-container{
        		margin-bottom: 15px;
        	}
        	.tracuusodoc .form-area h3{
        		margin-bottom: 10px;
        		text-align: center;
        		color: #fff;
        		text-transform: uppercase;
    		    font-size: 20px;
        	}
        	.tracuusodoc .form-area .form-group {
        		padding-left: 20px;
    			padding-right: 20px;
        	}
        </style>
        <div class="tracuusodoc">
	        <div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
				    <div class="form-area">
				        <form role="form">
		                    <h3><?php echo $title; ?></h3>
		    				<div class="form-group">
		    					<label for="inp_username">Username</label>
								<input type="text" class="form-control" id="inp_username" name="inp_username" placeholder="Username" required>
							</div>
							<div class="form-group">
								<label for="inp_password">Password</label>
								<input type="password" class="form-control" id="inp_password" name="inp_password" placeholder="Password" required>
							</div>
							<div class="form-group">
								<label for="makh">Mã khách hàng</label>
								<input type="text" class="form-control" id="makh" name="makh" placeholder="Mã Khách hàng" required>
							</div>
							<div class="row text-center btn-container">
				        		<button type="button" id="submit-btn" name="submit" class="btn btn-primary">TRA CỨU</button>
				        	</div>
				        </form>
				    </div>
				</div>
			</div>
		</div>
        <?php

        echo $after_widget;
    }

    public function create_newestpost_widget()
    {
        register_widget('WidgetNewestPost');
    }
}

new WidgetNewestPost();