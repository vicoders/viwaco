<?php
/*Template Name: News Page*/
get_header();
?>

<!--==========================
    =            Main            =
    ===========================-->
    <div id="news_list">
        <div class="container">
            <ol class="breadcrumb">
                <li>
                    <a href="#">Trang chủ </a>
                </li>
                <li class="active">Tin tức</li>
            </ol>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-9" id="main-content">
                    <section id="list">
                        <h2>Tin tức</h2>

							<?php news_post_list()?>

	                    </section>
	                    <!-- end list -->
	                </div>

	                <!-- end sm9 -->

	                <?php get_sidebar('other');?>

	            </div>
	            <!-- end row -->
	        </div>
	        <!-- end container -->
	    </div>
	    <!-- end newlist -->

	<?php get_footer();?>